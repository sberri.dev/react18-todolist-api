import React, { useState } from "react";
import axios from 'axios';
import "./App.css";
import TodoList from "./components/TodoList";

//Appel de l'API grâce à l'url ci-dessous
const API_URL = "https://jsonplaceholder.typicode.com/todos";

function App() { // Définition du composant principal App par une fonction 

  // Déclaration de l'état 'todos' à l'aide de la méthode useState avec un tableau vide
  const [todos, setTodos] = useState([]);

  // Utilisation de useEffect pour effectuer une action après le rendu initial du composant
  React.useEffect(() => {

    // Utilisation de la bibliothèque axios pour effectuer une requête GET vers l'API JSONPlaceholder
    axios.get(API_URL).then((response) => {

      // Mise à jour de l'état 'todos' avec les données (data) de la réponse de l'API
      setTodos(response.data);

    });
  }, []);

  // Rendu du composant principal
  return (

    // définition de la className de la fonction
    <div className="App">

      {/*Appel du composant TodoList en passant l'état 'todos' en tant que prop*/}
      <TodoList todos={todos} />
    </div>
  );
}

export default App;
